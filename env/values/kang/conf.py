CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "kang-alpha2",
        "PORT": "5432",
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "kang",
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://f438d38a9cff49818449817740585a11@sentry.livspace.com/84"
    }
}

GATEWAY = {
    "HOST": "axle",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "CIVITAS": {
        "HOST": "civitas.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/civitas'
        },
    },
    "CAULDRON": {
        "HOST": "cauldron.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/cauldron'
        },
    },
    "LAUNCHPAD": {
        "HOST": "launchpad-backend.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/launchpad'
        }
    },
    "CHAT": {
        "HOST": "api.alpha2.livspace.com/chat",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "KEY": "b20eb4e2-fb41-11e6-bc64-92361f002671",
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/chat'
        }
    },
    "BOUNCER": {
        "HOST": "bouncer.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json",
            "X-CLIENT-ID": "LAUNCHPAD",
            "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    },
    "CUSTOMER": {
        "HOST": "livspace-web-backend.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json",
            "X-CLIENT-ID": "LAUNCHPAD",
            "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
        },
        "GATEWAY": {
            "ENABLED": True,
                "PATH": '/livspace-web-backend'
        }
    },
    "GOOGLE": {
        "CALENDAR": {
            "ENABLED": True,
            "CREDENTIALS": {
                "type": "service_account",
                "project_id": "calendar-271113",
                "private_key_id": "64a72d0fcd694e545e523315ab5b7420eb2751fe",
                "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCXBUx/QDAGBBUB\naH+3ATdSJA16LVec2iJn1XWkCH2McNA917nQKgLOqDJXPf555hEyQMCIZB9Tjfz/\nxz4q6Caw8FwPG5XWLEY8jNQa96NvLX6RspneduNWLx3WFSIsLeD5LhhB/dtPl1xF\nfuBqDi8S3vsAjE8pMNQH6JZyao8GJvtIw8fccs0yakH20yLDL6Yib75apCz2NHvS\nzvepcRacREtbxVWXQI7KauZnMZAn8mlrnY7uOrryT3YelsQewiW6XMB5Csvi326N\nGoDMswxhBlhZfULMVrAgoJzZWFBS9Py/6yBebrALDhd3gSdjHm+HBIze3xDL9iXi\nhVfMS1ZvAgMBAAECgf8oYF6+GKQTolkXzmbIv8Nf+dHZgyl3qb366GyLbp0+7yNo\ngdLzKg9dMSkn8TJRLBU0BP90zRchP7idjYsRe+OG+S4Vhp/eMU15jYK2JNSpyGOc\nyXjLyMSNYrYlow8OFhmeTHu+DJLO7ajaQRG+12FMxsLGuBIARpv38xrw0YyIGCVM\nDZjIU75rV3sN8Avb0szI8CHjMhpbdAsZ6lackPy7yc7Bk8OVnbSqATtpBFthg1gQ\nUcTv4/oKWH/Q/9dLCkOKUTBky1HTTxCfb+rgWz90VzCSzdoW/kjEtqGjkOrCCyPK\nTrAjWc8YzjMBmZ7jEYlYGO07WLIOUc+4BtHzNkECgYEA0WSQZW9oLmzeZlLaOLLE\ngh7Z8NiV1e+z+xSBkb680/k4AwgpbPZ5fP6Z2wr6euJLlQmhulXXSaHccQ7SgrKP\nIMYCnNTq1+5wAUpbBDnZ9lnP4MaYCBs/T7yeU4T+LnMxFFXh4zjNdJF64XFfGXXx\nGa5azyhZgWBTNHl/yyNjZXUCgYEAuKKgP7dMzq5ehIxhriaEGxbZ2jZwdCBD/daP\nfosPhurz0RhnQwlUh5sHdXG5ncH7ZBhv/MdgBloMbgODCyJBJNyRl9bjhMELrlj5\nT1lN0OrUeSRfe4EMAN3Ir+hkhTvBy/HsBXRyDb+WrMEQv6Mec+Ms4kzF1Pr05MyJ\nWgux+9MCgYEAuTWqQw53qBjD3t4KB3yYND/1pgLtBX4wRDWvJasAer5FvOjLO4YH\nj3fjvsvHIkpncDxEwPyMoKR6x8XEjbZCw0phpTbvQm+0wm9ychp5oF1wcfE3KHRx\nW6YKpPVNnuhk0Qfl9YWYtTK47g/JfkvEJmMCD5oxES6SaifN0XBLysECgYBjdrBP\nQsasvMCMNTfV4kfedAPuCs+vr0YRc8mfiPhXXTD1nmjacydHWhu9iANPWhW8SXHi\nweEX6uCxT1ED6XMgkXYZTxkIBhrt1F6SAXDrL+ZAUHjjhpRZzyByqwWjxJpU7MGR\n+rYKdACOlf8YzZ31Hheq3WLhpkPREeKYUJ6UIwKBgQCnqwhX7+9NR/k7Napvyj3a\nuKEbV7Y0LO4yAhg0EwJFMj8sBczJN+1YhbcchXxpsyc4Vs9tLCO0/N6tXrA1V0Yw\nHSXnJ0ZJs/IQb2H3C0LalolWxOmfOZgJjShQuSI/JaSO5SQWqetpqwwAG3ezGS3U\nfmBHKZLKGNMSu8PjMZ/pSA==\n-----END PRIVATE KEY-----\n",
                "client_email": "canvas@calendar-271113.iam.gserviceaccount.com",
                "client_id": "114486538617170790186",
                "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                "token_uri": "https://oauth2.googleapis.com/token",
                "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/canvas%40calendar-271113.iam.gserviceaccount.com"
            }
        }
    }
}