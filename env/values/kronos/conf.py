CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "kronos-alpha2",
        "PORT": "5432",
    },
    "LAUNCHPAD-DB": {
        "HOST": "launchpad-db",
        "USERNAME": "livspace",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "launchpad_backend",
        "PORT": "3306",
    },    
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "AWS": {
        "S3": {
            "ACCESS_KEY": "AKIARPQNMXYNA3UJAW46",
            "SECRET_KEY": "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",
            "XYLOOP_BUCKET_NAME": "livspace-ext-xyloop",
            "FOLDER_NAME": "test"
        },
        "ATHENA": {
            "ACCESS_KEY": "AKIARPQNMXYNNFH3KVGY",
            "SECRET_KEY": "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",
            "REGION_NAME": "ap-southeast-1",
            "STAGING_DIR": "s3://aws-athena-query-results-102033571354-ap-southeast-1"
        }
    },
    "MIGRATE_XYLOOP_DATA": False,
    "ENABLE_CRITERIA_VALIDATION": True,
    "ENABLE_NODE_CHANGE_EVENT": True,    
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "kronos"
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "local",
        "DSN": "https://a29025aa42a84676a500bc8abbb7a798@sentry.livspace.com/76"
    },
    "CACHE": {
        "NAME": "REDIS",
        "ENABLED": True,
        "REDIS": {
            "HOST": "redis-kronos",
            "PORT": 26379,
            "SOCKET_TIMEOUT": 0.1,
            "PASSWORD": "",
            "PREFIX": "kronos-alpha2",
            "USE_SENTINEL": True,
            "SENTINEL_SERVICE_NAME": "redis-kronos"
        }
    }
}

GATEWAY = {
    "HOST": "api.alpha2.livspace.com",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        "HOST": "launchpad-backend",
        "HEADERS": {},
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/launchpad-backend'
        }
    },
    "BOUNCER": {
        "HOST": "bouncer",
        "HEADERS": {},
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    },
    "CAULDRON": {
        "HOST": "cauldron",
        "HEADERS": {},
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/cauldron'
        }
    },
    "BACKOFFICE": {
        "HOST": "backoffice",
        "HEADERS": {
            "X-Auth-Token": "8cQ904440221g0kWq8wE7N68T48DcfMr",
            "X-Requested-By": "0"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/backoffice'
        }
    },
    "CARBON": {
        "HOST": "carbon",
        "HEADERS": {},
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/carbon'
        }
    },
    "KAMARTAJ": {
        "HOST": "kamar-taj",
        "HEADERS": {},
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/kamar-taj'
        }
    },
    "CALENDAR": {
        "HOST": "calendar",
        "HEADERS": {
            "X-Requested-By": "KRONOS",
            "X-Request-Id": "0"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/kamar-taj'
        }
    },
    "FMS": {
        "HOST": "fms",
        "HEADERS": {
            "X-Auth-Token": "JJi3K9Qc9slKsl60mgiK7VTsNDiV7W1NpJV3",
            "X-Requested-By": "1",
            "X-Request-Id": "aapo--763450981-dd45"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/fms'
        }
    }
}
