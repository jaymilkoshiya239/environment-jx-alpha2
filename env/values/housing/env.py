from .base_settings import *

ALLOWED_HOSTS = ['*']
SECRET_KEY = '^&jqyid@@mgjqo%a$4ng%-j%pw@e==3ue%_yy!jgzgoef-xfh1'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'housing',
        'USER': 'livspace',
        'PASSWORD': 'Livspace123Stage',
        'HOST': 'livspace-db',
        'PORT': '3306',
    }
}

BOUNCER = {
    'HOST': 'bouncer.alpha2.livspace.com',
    'HEADERS': '{"X-CLIENT-ID":"LAUNCHPAD","X-CLIENT-SECRET":"6ee38134-64c0-4095-af8a-38f05e69b0bd"}',
    'id': 20,
    'app': 'LAUNCHPAD',
    'secret': '6ee38134-64c0-4095-af8a-38f05e69b0bd',
    'COOKIE_NAME': 'B',
    'EMPLOYEE_COOKIE_KEY': '770A8A65DA156D24EE2A093277530142',
    'REQUESTER_TOKEN_KEY': '770A8A65DA156D24EE2A093277530142',
    'COOKIE_DOMAIN': 'lpdev.livspace.com',
    'COOKIE_PATH': '/',
    'REFRESH_TIME_IN_MINUTE': 30,
    'COOKIE_MAX_AGE': 3600,
}

LAUNCHPAD_URL = "launchpad-backend"
LIVHOME_URL = "livhome-backend"
TOOL_HOST = "http://livhome"
TOOL_AUTH_TOKEN = "30a42e59-1c05-4faf-b309-45aaece28015"

# Have to add correct values
URI = "http://housing.alpha2.livspace.com"

try:
    from .local import *
except ImportError:
    pass

USE_TZ = True

ENV = 'Dev'
GRAPPELLI_ADMIN_TITLE = 'Housing Web Admin (alpha2)'

# AWS s3 bucket settings

STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
AWS_DEFAULT_ACL = 'public-read'
AWS_ACCESS_KEY_ID = 'AKIARPQNMXYNPB5GOP5R'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'livspace.website.test'
# AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_CUSTOM_DOMAIN = 'd1z63ui8y2ntmx.cloudfront.net'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}

# Elastic Search Configuration
AWS_ES_HOST_REGION = 'ap-southeast-1'
AWS_ES_HOST = 'search-lsweb-nkfgkbcz7bcekepwutkfr2ftga.ap-southeast-1.es.amazonaws.com'
AWS_ELASTIC_ACCESS_KEY = 'AKIARPQNMXYNPB5GOP5R'
AWS_ELASTIC_SECRET_KEY = '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'


ES_PROPERTY_INDEX = 'alpha2_property_index'
ES_SEARCH_QUERY_FUZZINESS = 1
ES_SEARCH_RESULT_LIMIT = 10

BOOST_BUILDING_IDS = [4]



EVENT_SYSTEM_ENVIRONMENT = 'local'

# Google API Config
GOOGLE_MAP_HOST = 'maps.googleapis.com/maps/api'
GOOGLE_MAP_API_KEY = 'AIzaSyAN85tzrovp9RrvB0ZGoYrd67QnO7k0pUU'


# Logging Properties
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '{asctime} {levelname} {module} {filename} {lineno} - {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'application': {
            'handlers': ['console'],
            'propagate': True,
        },
        'root': {
            'handlers': ['console'],
            'propagate': True,
        },
    },
}
